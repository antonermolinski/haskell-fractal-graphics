#** Развертывание окружения **#

1. Скачать и установить stack.
2. git clone https://antonermolinski@bitbucket.org/antonermolinski/haskell-fractal-graphics.git, cd haskell-fractal-graphics
3. Настроить ghc. Для этого необходимо вызвать stack setup, которая либо найдет компилятор в переменной PATH, либо загрузит его в глобальную директорию stack (по-умолчанию ~./stack).
4. Собрать проект, вызвав stack build. Это необходимо делать каждый раз перед запуском.
5. Запустить проект командой stack exec haskell-fractal-graphics -- ARG, где ARG - поколение фрактала