
import Graphics.Gloss
import System.Environment

main = do
  [gen] <- getArgs
  display (InWindow "AntiSnowflake" (500, 500) (20,  20))
               black (picture (read gen::Int))


-- Fix a starting edge length of 360
edge = 360 :: Float


-- Move the fractal into the center of the window and color it nicely
picture :: Int -> Picture
picture degree 
        = Color azure
        $ Translate (-edge/2) (-edge * sqrt 3/6)
        $ antiSnowflake degree
        

-- The fractal function
side :: Int -> Picture
side 0 = Line [(0, 0), (edge, 0)]
side n 
 = let  newSide = Scale (1/3) (1/3) 
                $ side (n-1)
   in   Pictures
                [ newSide
                , Translate (edge/3) 0                          $ Rotate (-60) newSide 
                , Translate (3 * edge/6) ((edge/3 * sqrt 3)/2)  $ Rotate 60 newSide 
                , Translate (2 * edge/3) 0                      $ newSide ]


-- Put 3 together to form the antiSnowflake
antiSnowflake :: Int -> Picture
antiSnowflake n 
 = let  oneSide = side n
   in   Pictures
                [ oneSide 
                , Translate edge 0                      $ Rotate (-120) $ oneSide
                , Translate (edge/2) (edge * sqrt 3/2)  $ Rotate 120    $ oneSide]



